﻿using Xamarin.Forms;

namespace ListViewStuff
{
    public partial class ListViewStuffPage : ContentPage
    {
        public ListViewStuffPage()
        {
            InitializeComponent();
        }
   

        void Handle_NavigateToWebNavigationListView(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new GoToWebsite());
        }

    }
}

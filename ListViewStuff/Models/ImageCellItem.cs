﻿using System;
using Xamarin.Forms;

namespace ListViewStuff.Models
{
    public class ImageCellItem
    {
        public string ImageText
        {
            get;
            set;
        }

        public ImageSource IconSource
        {
            get;
            set;
        }

        public string url
        {
            get;
            set;
        }

        public string SecondaryText
        {
            get;
            set;
        }
        //    new ImageCellItem()
        //    {
        //        IconSource = ImageSource.FromFile("BullShark.jpg"),
        //                ImageText = "Bull Shark",
        //                SecondaryText = "Toro!",
        //                url = "https://en.wikipedia.org/wiki/Bull_shark",
        //            },
        //            new ImageCellItem()
        //    {
        //        IconSource = ImageSource.FromFile("GreatWhite.jpeg"),
        //                ImageText = "Great White Shark",
        //                SecondaryText = "Hungry as ever I see",
        //                url = "https://en.wikipedia.org/wiki/Great_white_shark",
        //            },
        //            new ImageCellItem()
        //    {
        //        IconSource = ImageSource.FromFile("MakoShark 2.png"),
        //                ImageText = "Mako Shark",
        //                SecondaryText = "Not messing with this guy",
        //                url = "https://en.wikipedia.org/wiki/Shortfin_mako_shark",
        //            },
        //            new ImageCellItem()
        //    {
        //        IconSource = ImageSource.FromFile("HammerheadShark.png"),
        //                ImageText = "Hammerhead Shark",
        //                SecondaryText = "Such a cutie",
        //                url = "https://en.wikipedia.org/wiki/Hammerhead_shark",
        //            },
        //            new ImageCellItem()
        //    {
        //        IconSource = ImageSource.FromFile("TigerShark.jpg"),
        //                ImageText = "Tiger Shark",
        //                SecondaryText = "Would definitely rip apart our mascot",
        //                url = "https://en.wikipedia.org/wiki/Tiger_shark",
        //            },
        //            new ImageCellItem()
        //    {
        //        IconSource = ImageSource.FromFile("BaskingShark.jpeg"),
        //                ImageText = "Basking Shark",
        //                SecondaryText = "Do these really exist?",
        //                url = "https://en.wikipedia.org/wiki/Basking_shark",
        //            },
        //            new ImageCellItem()
        //    {
        //        IconSource = ImageSource.FromFile("WhaleShark.jpeg"),
        //                ImageText = "Whale Shark",
        //                SecondaryText = "Massive but friendly",
        //                url = "https://en.wikipedia.org/wiki/Whale_shark",
        //            },
        //            new ImageCellItem()
        //    {
        //        IconSource = ImageSource.FromFile("megalodonshark 2.jpg"),
        //                ImageText = "Megaladon Shark(compared to T-Rex)",
        //                SecondaryText = "Legit scared just looking at it",
        //                url = "https://en.wikipedia.org/wiki/Megalodon",
        //            },
        //}
    }
}


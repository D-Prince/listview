﻿using System;
using System.Collections.Generic;
using ListViewStuff.Models;

using Xamarin.Forms;

namespace ListViewStuff
{
    public partial class MoreInformationPage : ContentPage
    {
        public MoreInformationPage()
        {
            InitializeComponent();
        }
    
        public MoreInformationPage(Shark shark)
        {
            InitializeComponent();

            BindingContext = shark;
        }

        void ButtonClicked(object sender, System.EventArgs e)
        {
            var buttonUrl = (Button)sender;
            var itemTapped = (string)buttonUrl.Text;
            var uri = new Uri(itemTapped);
            Device.OpenUri(uri);
           
        }
    }
}

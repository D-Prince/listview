﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ListViewStuff.Models;
using Xamarin.Forms;

namespace ListViewStuff
{
    public partial class GoToWebsite : ContentPage
    {
        public GoToWebsite()
        {
            InitializeComponent();

            PopulateListView();
        }


        private void PopulateListView()
        {
            var sharkBait = new ObservableCollection<Shark>();

            var shark1 = new Shark
            {
                Name = "Bull Shark",
                Size = "Medium Sized",
                moreInfo = "https://en.wikipedia.org/wiki/Bull_shark",
            };
            sharkBait.Add(shark1);

            var shark2 = new Shark
            {
                Name = "Great White Shark",
                Size = "Large Sized",
                moreInfo = "https://en.wikipedia.org/wiki/Great_white_shark",
            };
            sharkBait.Add(shark2);

            var shark3 = new Shark
            {
                Name = "Mako Shark",
                Size = "Medium Sized",
                moreInfo = "https://en.wikipedia.org/wiki/Shortfin_mako_shark",
            };
            sharkBait.Add(shark3);

            var shark4 = new Shark
            {
                Name = "Hammerhead Shark",
                Size = "Medium Sized",
                moreInfo = "https://en.wikipedia.org/wiki/Hammerhead_shark",
            };
            sharkBait.Add(shark4);

            var shark5 = new Shark
            {
                Name = "Tiger Shark",
                Size = "Small/Medium Sized",
                moreInfo = "https://en.wikipedia.org/wiki/Tiger_shark",
            };
            sharkBait.Add(shark5);

            var shark6 = new Shark
            {
                Name = "Basking Shark",
                Size = "Large Sized",
                moreInfo = "https://en.wikipedia.org/wiki/Basking_shark",
            };
            sharkBait.Add(shark6);

            var shark7 = new Shark
            {
                Name = "Whale Shark",
                Size = "Xtra Large Sized",
                moreInfo = "https://en.wikipedia.org/wiki/Whale_shark",
            };
            sharkBait.Add(shark7);

            var shark8 = new Shark
            {
                Name = "Megalodon Shark",
                Size = "King Sized",
                moreInfo = "https://en.wikipedia.org/wiki/Megalodon",
            };
            sharkBait.Add(shark8);
             


            WebsiteListView.ItemsSource = sharkBait;
        }

        void moreButtonClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var shark = (Shark)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreInformationPage(shark));
        }

        void deleteButtonClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var shark = (Shark)menuItem.CommandParameter;
            new ObservableCollection<Shark>().Remove(shark);


           //WebsiteListView.ItemsSource = remove;
        }

        void HandleRefreshing(object sender, System.EventArgs e)
        {
            PopulateListView();
            WebsiteListView.IsRefreshing = false;
        }
    }
}
